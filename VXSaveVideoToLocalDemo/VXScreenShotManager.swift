//
//  VXScreenShotManager.swift
//  VXSaveVideoToLocalDemo
//
//  Created by zhangxin on 2020/8/18.
//  Copyright © 2020 zhangxin. All rights reserved.
//  截图保存成视频

import UIKit

class VXScreenShotManager: NSObject {
    private static let subsInstance = VXScreenShotManager()
    //私有化构造方法
    private override init() { }
    //提供静态访问方法
    @objc public static var shared: VXScreenShotManager {
        return self.subsInstance
    }
    class func shotInView(_ view:UIView) ->UIImage {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, true, 0.0)
        let context = UIGraphicsGetCurrentContext()
        view.layer.render(in: context!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        context?.flush()
       // UIGraphicsBeginImageContext(view.bounds.size,true,0.0)
//        view.layer.render(in: UIGraphicsGetCurrentContext()!)
//        let image = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsGetCurrentContext()
        return image ?? UIImage()
    }
    typealias WriteCallBack = (_ images:[UIImage]) ->()
    func writeImageToArray(_ time:Int,_ view:UIView,wiriteCallBack:WriteCallBack?) {
        var templeDataArray : [UIImage] = []
        var i = 1
        var timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (templeTimer) in
            let image = VXScreenShotManager.shotInView(view)
            if image != nil && i < Int((CGFloat(time) / 0.1)) {
                i = i + 1
                templeDataArray.append(image)
            } else {
                //屏幕截图完成，图片数量 = time / 截图频率
                templeTimer.invalidate()
                //templeTimer = nil
                wiriteCallBack?(templeDataArray)
            }
        }
    }
    typealias ScreenInViewCallBack = (_ videoPath:String) ->()
    //MARK: - 录制屏幕，并保存到相册
    //截屏获取图片，再把图片转成视频
    class func recordScreenInView(_ view:UIView,_ time:Int,_ screenCallBack:ScreenInViewCallBack?) {
        VXScreenShotManager.shared.writeImageToArray(time, view) { (images) in
            //获得截图数组,把图片生成视频
            VXSaveVideoManager.convertImagesToVideo(images,time) { (pathStr) in
                print("截图视频保存本地成功："+pathStr)
                //再保存到相册
                let url = URL(fileURLWithPath: pathStr ?? "")
                VXSaveVideoManager.saveVideoToAlbum(url) { (isSuccess, isPermission) in
                    if isSuccess == true {
                        print("视频成功保存到相册")
                        screenCallBack?(pathStr)
                    }
                }
            }
        }
    }

}
