//
//  ViewController.swift
//  VXSaveVideoToLocalDemo
//
//  Created by zhangxin on 2020/8/17.
//  Copyright © 2020 zhangxin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var videoPath : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        var images : [UIImage] = []
        for i in 0..<22 {
            let name = String(format: "%d", i)
            let img = UIImage.init(named: name)
            images.append(img!)
        }
        weak var wself = self
        VXSaveVideoManager.convertImagesToVideo(images,100) { (pathStr) in
            print("哈哈哈哈哈哈哈哈哈哈"+pathStr)
            wself?.videoPath = pathStr
        }
    }


    @IBAction func saveAction(_ sender: Any) {
//        let url = URL(fileURLWithPath: self.videoPath ?? "")
//        VXSaveVideoManager.saveVideoToAlbum(url) { (isSuccess, isPermission) in
//            if isSuccess == true {
//                print("保存成功")
//            }
//        }
        VXScreenShotManager.recordScreenInView(self.view, 5) { (paths) in
            print("-------"+paths)
            
        }
        let alertVC = UIAlertController(title:"提示", message: "保存成功", preferredStyle: UIAlertController.Style.alert)
        let alertAction = UIAlertAction(title: "confirm", style: UIAlertAction.Style.cancel, handler: nil)
        alertVC.addAction(alertAction)
        self.present(alertVC, animated: true, completion: nil)
//        let testVC = VXTestVC()
//        testVC.title = "测试页面"
//        self.navigationController?.pushViewController(testVC, animated: true)
    }
}

