//
//  VXSaveVideoManager.swift
//  VXSaveVideoToLocalDemo
//
//  Created by zhangxin on 2020/8/17.
//  Copyright © 2020 zhangxin. All rights reserved.
//

import UIKit
import AVKit
import MediaPlayer
import AVFoundation
import CoreGraphics
import Photos

typealias VideoConvertCallBack = (_ pathStr:String)->()
// 获取应用名作为自定义相册名
let albumName : String = Bundle.main.infoDictionary?["CFBundleDisplayName"] as? String ?? ""

class VXSaveVideoManager: NSObject {
    
    //MARK: - 图片转视频
    //传入图片数组生产视频，返回视频路径(videoTime是预设视频的长度（秒））
    class func convertImagesToVideo(_ images:[UIImage],_ videoTime:Int,_ converCallBack:VideoConvertCallBack?){
        //设置图片尺寸
        let imageSize = CGSize(width: 320, height: 480)
        var templeImages : [UIImage] = []
        //对图片大小压缩
        for i in 0..<images.count {
            let image = images[i]
            var imageNew = self.scaledImage(image, imageSize)
            templeImages.append(imageNew)
        }
        //设置mov路径
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let firstStr = paths.first ?? ""
        let moviePath = firstStr + "vxlocal.mov"
        print("保存的路径："+moviePath)
        //视频的大小
        let videoSize = CGSize(width: 320, height: 480)
        var error : Error?
        //转成UTF-8
        let utf8Str =  (moviePath as NSString).utf8String
        unlink(utf8Str)
        print(utf8Str)
        var writeInitErr : Error? = nil
        do {
            let videoWriter = try AVAssetWriter(outputURL: URL(fileURLWithPath: moviePath), fileType: AVFileType.mov)
            //mov格式设置，编码格式 宽度和高度
            let videoSetInfo = [AVVideoWidthKey:videoSize.width,AVVideoCodecKey:AVVideoCodecType.h264,AVVideoHeightKey:videoSize.height] as [AnyHashable : Any]
            let writerInput = AVAssetWriterInput(mediaType: AVMediaType.video, outputSettings: videoSetInfo as! [String : Any])
            let sourcePixelBufferAttributesDic = [kCVPixelBufferPixelFormatTypeKey:kCVPixelFormatType_32ARGB]
            //    AVAssetWriterInputPixelBufferAdaptor提供CVPixelBufferPool实例,
            //    可以使用分配像素缓冲区写入输出文件。使用提供的像素为缓冲池分配通常
            //    是更有效的比添加像素缓冲区分配使用一个单独的池
            let adaptor = AVAssetWriterInputPixelBufferAdaptor(assetWriterInput: writerInput, sourcePixelBufferAttributes: sourcePixelBufferAttributesDic as [String : Any])
            if videoWriter.canAdd(writerInput) {
                print("1111")
            } else {
                print("222")
            }
            videoWriter.add(writerInput)
            videoWriter.startWriting()
            videoWriter.startSession(atSourceTime: .zero)
            //h合成多张图为一个视频文件
            var frame = -1
            let mediaInputQueue = DispatchQueue(label:"mediaInputQueue")
          //  var templeCallBack = converCallBack
             //这个10应该是等于 (FPS * 视频预设总时长)/图片数组总数
            let pixCount = 60 * videoTime / images.count
            writerInput.requestMediaDataWhenReady(on: mediaInputQueue, using: {
                () in
                while (writerInput.isReadyForMoreMediaData) {
                    frame = frame + 1
                    if frame >= templeImages.count * pixCount {
                        writerInput.markAsFinished()
                        videoWriter.finishWriting(completionHandler: {
                            ()in
                            print("视频合成完毕")
                            if converCallBack != nil {
                                converCallBack!(moviePath)
                            }
                        })
                        break;
                    }
                    
                    var buffer : CVPixelBuffer? = nil
                   
//                    let idx = frame / 10
                    let idx = frame / pixCount
                    buffer = self.pixelBufferFromCGImage(templeImages[idx].cgImage!, videoSize)
                    if buffer != nil {
                        //设置每秒播放的图片个数
                        if adaptor.append(buffer!, withPresentationTime: CMTime(value: CMTimeValue(frame), timescale: 60)) == nil {
                            print("fail")
                        } else {
                            print("ok")
                        }
                    }
                }
            })
        } catch {
            print("failed")
        }
    }
    
    //压缩图片尺寸
    class func scaledImage(_ image:UIImage,_ toSize:CGSize) ->UIImage {
        UIGraphicsBeginImageContext(toSize)
        image.draw(in: CGRect(x: 0, y: 0, width: toSize.width, height: toSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if newImage == nil {
            return image
        }
        return newImage!
    }
    
    class func pixelBufferFromCGImage(_ image:CGImage,_ size:CGSize) -> CVPixelBuffer {
        let options = [kCVPixelBufferCGImageCompatibilityKey:true,kCVPixelBufferCGBitmapContextCompatibilityKey:true]
        var pxbuffer : CVPixelBuffer? = nil
        CVPixelBufferCreate(kCFAllocatorDefault, Int(size.width), Int(size.height), kCVPixelFormatType_32ARGB, options as CFDictionary, &pxbuffer)
        CVPixelBufferLockBaseAddress(pxbuffer!, [])
        let pxdata = CVPixelBufferGetBaseAddress(pxbuffer!)
        let rgbcolorSpace = CGColorSpaceCreateDeviceRGB()
    //当你调用这个函数的时候，Quartz创建一个位图绘制环境，也就是位图上下文。当你向上下文中绘制信息时，Quartz把你要绘制的信息作为位图数据绘制到指定的内存块。一个新的位图上下文的像素格式由三个参数决定：每个组件的位数，颜色空间，alpha选项
        let context = CGContext(data: pxdata, width: Int(UInt(size.width)), height: Int(size.height), bitsPerComponent: 8, bytesPerRow: Int(4*size.width), space: rgbcolorSpace, bitmapInfo: 2)
       
        //使用CGContextDrawImage绘制图片  这里设置不正确的话 会导致视频颠倒
        //当通过CGContextDrawImage绘制图片到一个context中时，如果传入的是UIImage的CGImageRef，因为UIKit和CG坐标系y轴相反，所以图片绘制将会上下颠倒
        let templeRect : CGRect = CGRect(x: 0,y: 0,width: image.width,height: image.height)
        context?.draw(image, in: templeRect)
        //释放色彩空间
        //释放contenx
        /*
         // 释放色彩空间
            
            CGColorSpaceRelease(rgbColorSpace);
            
            // 释放context
            
            CGContextRelease(context);
            
         */
        context?.flush()
        CVPixelBufferUnlockBaseAddress(pxbuffer!, [])
        return pxbuffer!
    }
    
    
    //MARK: - 保存视频
    //保存视频到相册
    typealias VideoSaveHandler = (_ isSuccess:Bool,_ isPermisstions:Bool)->()
    class func saveVideoToAlbum(_ url:URL,_ handler:VideoSaveHandler?) {
        //是否有保存权限
        if self.achiveAuthorizationStatis() == false {
            handler?(false,false)
        }
        let collection = self.createCollection()
        let createAssets = self.createAssetsWithVideoUrl(url)
        if createAssets == nil || collection == nil {
            handler?(false,true)
            return
        }
        var error : Error? = nil
        do {
            try PHPhotoLibrary.shared().performChangesAndWait {
                let collectionChangeRequest = PHAssetCollectionChangeRequest(for: collection)
                collectionChangeRequest?.insertAssets(createAssets, at: NSIndexSet(index: 0) as IndexSet)
                handler?(true,true)
            }
        } catch  {
            handler?(false,true)
            print("视频保存失败")
        }
    }
    
    class func achiveAuthorizationStatis() ->Bool {
        // 1、判断相册访问权限
        /*
         * PHAuthorizationStatusNotDetermined = 0, 用户未对这个应用程序的权限做出选择
         * PHAuthorizationStatusRestricted, 此应用程序没有被授权访问的照片数据。可能是家长控制权限。
         * PHAuthorizationStatusDenied, 用户已经明确拒绝了此应用程序访问照片数据.
         * PHAuthorizationStatusAuthorized, 用户已授权此应用访问照片数据.
         */
        let status = PHPhotoLibrary.authorizationStatus()
        return !(status == PHAuthorizationStatus.denied || status == PHAuthorizationStatus.restricted)
    }
    //获取应用相册
    class func createCollection() ->PHAssetCollection {
        //获得所有的自定义相册
        let collections = PHAssetCollection.fetchAssetCollections(with: PHAssetCollectionType.album, subtype: PHAssetCollectionSubtype.albumRegular, options: nil)
        for i in 0..<collections.count {
            let collect : PHAssetCollection = collections[i]
            if collect.localizedTitle == albumName {
                return collect
            }
        }
        
        //没有自定义相册
        var createCollectionId : String? = nil
        //创建相册
        do {
            try PHPhotoLibrary.shared().performChangesAndWait {
                ()in
               createCollectionId = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: albumName).placeholderForCreatedAssetCollection.localIdentifier
                //createCollectionId = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: albumName)
            }
        } catch  {
        }
        if createCollectionId == nil {
            return PHAssetCollection()
        }
        //取出创建好的相册
        let templeCollect = PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [createCollectionId!], options: nil).firstObject
        if templeCollect != nil {
            return templeCollect!
        }
        return PHAssetCollection()
        
    }
    
    //获得添加到【相机胶卷】中的视频
    class func createAssetsWithVideoUrl(_ videoUrl:URL) ->PHFetchResult<PHAsset> {
        var createAssetId : String? = nil
        do {
            try PHPhotoLibrary.shared().performChangesAndWait {
                createAssetId = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoUrl)?.placeholderForCreatedAsset?.localIdentifier
            }
        } catch  {
            
        }
        if createAssetId == nil {
            return PHFetchResult()
        }
        let asset : PHFetchResult = PHAsset.fetchAssets(withLocalIdentifiers: [createAssetId!], options: nil)
        return asset
        
    }
}
